// Import module dependencies
const accountController = require("../controllers/accountController");
const talentController = require("../controllers/talentController");
// Export function that accepts Express app as an argument
module.exports = function (app) {
  // Route for talent
  app.post("/addAccount", accountController.addAccount);
  app.post("/talent/:id/course",talentController.addCourseTalent);
  app.post("/talent/:id/projectexp",talentController.addProjectExpTalent);
  app.post("/talent/:id/education",talentController.addEducationTalent);
  app.post("/talent/:id/employment",talentController.addEmploymentTalent);
  app.post("/editProfile/:id", talentController.updateProfile);
  app.get("/talent", talentController.getTalentByStatus);
}