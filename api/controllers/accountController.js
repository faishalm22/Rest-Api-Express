const mongoose = require('mongoose');
const Account = require('../models/accountModel');
const Talent = require('../models/talentModel');
const nodemailer = require('nodemailer');
const config = require('../../config/config');

exports.addAccount = async (req, res, next) => {
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const { email, full_name, phone_number, company_name } = req.body;
    if (!email || !full_name || !company_name) {
      await session.abortTransaction();
      session.endSession();
      return res.status(400).json({ message: 'Email, full name, and company name are required' });
    }

    const password = Math.random().toString(36).slice(-8);

    let existingAccount = await Account.findOne({ email: email }).session(session);
    if (existingAccount) {
      await session.abortTransaction();
      session.endSession();
      return res.status(409).json({ message: 'Account with this email already exists' });
    }

    const newAccount = new Account({
      email: email,
      full_name: full_name,
      phone_number: phone_number || '',
      password: password,
      user_id: null,
      role: 'talent'
    });

    await newAccount.save({ session });

    const newTalent = new Talent({
      status: 'available',
      company: company_name
    });

    await newTalent.save({ session });

    newAccount.user_id = newTalent._id;
    await newAccount.save({ session });

    const transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: config.email_address,
        pass: config.email_password
      }
    });

    const mailOptions = {
      from: config.email_address,
      to: email,
      subject: 'Your Talent account password',
      text: `Hello! ${full_name}Your Talent account has been created. Your password is: ${password}`
    };

    await transporter.sendMail(mailOptions);

    await session.commitTransaction();
    session.endSession();

    return res.status(201).json({ message: 'Talent created successfully' });
  } catch (err) {
    console.error(err);
    await session.abortTransaction();
    session.endSession();
    return res.status(500).json({ message: 'Server error' });
  }
};
