const mongoose = require('mongoose');
const Talent = require('../models/talentModel');
const Course = require('../models/courseModel');
const Employment = require('../models/employmentModel');
const Hardskill = require('../models/hardskillModel');

exports.updateProfile = async (req, res) => {
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const { full_name,place_of_birth, date_of_birth, religion, gender, health, language, company, hardskill } = req.body;
    const { id } = req.params;

    const talent = await Talent.findById(id).session(session);
    if (!talent) {
      await session.abortTransaction();
      session.endSession();
      return res.status(404).json({ message: 'Talent not found' });
    }

    talent.full_name = full_name || talent.full_name;
    talent.place_of_birth = place_of_birth || talent.place_of_birth;
    talent.date_of_birth = date_of_birth || talent.date_of_birth;
    talent.religion = religion || talent.religion;
    talent.gender = gender || talent.gender;
    talent.health = health || talent.health;
    talent.language = language || talent.language;
    talent.company = company || talent.company;

    if (hardskill) {
      const { framework, application_server, database, operating_system, development_tools, programming_language } = hardskill;

      let hardskillData = await Hardskill.findOne({ 
        framework, 
        application_server, 
        database, 
        operating_system, 
        development_tools, 
        programming_language 
      }).session(session);

      if (!hardskillData) {
        hardskillData = new Hardskill({
          framework,
          application_server,
          database,
          operating_system,
          development_tools,
          programming_language
        });

        await hardskillData.save({ session });
      }

      talent.hardskill = [hardskillData._id];
    }

    await talent.save({ session });
    await session.commitTransaction();
    session.endSession();

    return res.status(200).json({ message: 'Talent profile updated successfully' });
  } catch (err) {
    console.error(err);
    await session.abortTransaction();
    session.endSession();
    return res.status(500).json({ message: 'Server error' });
  }
};


exports.getAllTalent = async (req, res) => {
  try {
    const talents = await Talent.find()
      .populate('hardskill')
      .populate('courses')
      .populate('projectexp')
      .populate('education');
    res.status(200).json({
      status: 'success',
      data: {
        talents
      }
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal server error'
    });
  }
};

exports.addCourseTalent = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, provider, date, duration } = req.body;

    const talent = await Talent.findById(id);
    if (!talent) {
      return res.status(404).json({
        status: 'fail',
        message: 'Talent not found'
      });
    }

    const newCourse = new Course({ title, provider, date, duration });
    await newCourse.save();
    talent.courses.push(newCourse._id);
    await talent.save();
    res.status(201).json({
      status: 'success',
      data: {
        talent
      }
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal server error'
    });
  }
};

exports.addEducationTalent = async (req, res) => {
  try {
    const { id } = req.params;
    const { school, degree, subject } = req.body;

    const talent = await Talent.findById(id);
    if (!talent) {
      return res.status(404).json({
        status: 'fail',
        message: 'Talent not found'
      });
    }

    const newEducation = new Education({ school, degree, subject });
    await newEducation.save();
    talent.education.push(newEducation._id);
    await talent.save();
    res.status(201).json({
      status: 'success',
      data: {
        talent
      }
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal server error'
    });
  }
};

exports.addProjectExpTalent = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, provider, date, duration } = req.body;

    const talent = await Talent.findById(id);
    if (!talent) {
      return res.status(404).json({
        status: 'fail',
        message: 'Talent not found'
      });
    }
    const newProjectExp = new ProjectExp({ title, provider, date, duration });
    await newProjectExp.save();
    talent.projectexp.push(newProjectExp._id);
    await talent.save();
    res.status(201).json({
      status: 'success',
      data: {
        talent
      }
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal server error'
    });
  }
};

exports.addEmploymentTalent = async(req, rest) => {
  try {
    const { id } = req.params;
    const { company_name, work_from, until, role } = req.body;

    const talent = await Talent.findById(id);
    if (!talent) {
      return res.status(404).json({
        status: 'fail',
        message: 'Talent not found'
      });
    }

    const newEmployment = new Employment({ company_name, work_from, until, role });
    await newEmployment.save();
    talent.employment.push(newEmployment._id);
    await talent.save();
    res.status(201).json({
      status: 'success',
      data: {
        talent
      }
    });    
  } catch (err){
    console.error(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal server error'
    });
  }
}

exports.getTalentByStatus = async (req, res) => {
  const { status } = req.query;

  try {
    let query = {
      status: status
    };

    const talents = await Talent.find(query)
      .populate('hardskill');
    res.status(200).json({
      status: 'success',
      data: {
        talents
      }
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      message: 'Internal server error'
    });
  }
};
