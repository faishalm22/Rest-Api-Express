const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const accountSchema = new Schema({
  email: { type: String, required: true },
  full_name: { type: String, required: true },
  phone_number: { type: String, default: '' },
  password: { type: String, required: true },
  user_id: { type: Schema.Types.ObjectId, refPath: 'role'},
  role: { type: String, enum: ['talent', 'bizdev','recruiter'], required: true }
});

const Account = mongoose.model('Account', accountSchema);

module.exports = Account;
