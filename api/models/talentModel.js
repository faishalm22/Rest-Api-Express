const mongoose = require('mongoose');
const { Schema } = mongoose;

const talentSchema = new Schema({
  full_name: {
     type: String,
     required: true 
  },
  date_of_birth: {
    type: Date,
    default: null
  },
  religion: {
    type: String,
    default: ''
  },
  place_of_birth: {
    type: String,
    default: ''
  },
  gender: {
    type: String,
    default: ''
  },
  health: {
    type: String,
    default: ''
  },
  language: {
    type: [String],
    default: ''
  },
  company: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    enum: ['available', 'not available'],
    default: 'available'
  },
  hardskill: [{
    type: Schema.Types.ObjectId,
    ref: 'Hardskill'
  }],
  courses: [{
    type: Schema.Types.ObjectId,
    ref: 'Course'
  }],
  projectexp: [{
    type: Schema.Types.ObjectId,
    ref: 'ProjectExp'
  }],
  education: [{
    type: Schema.Types.ObjectId,
    ref: 'Education'
  }],
  employment: [{
    type: Schema.Types.ObjectId,
    ref: 'Employment'
  }]
});

const Talent = mongoose.model('Talent', talentSchema);

module.exports = Talent;

