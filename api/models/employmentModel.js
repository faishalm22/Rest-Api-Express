const mongoose = require('mongoose');
const { Schema } = mongoose;

const employmentSchema = new Schema({
  company_name: {
    type: String,
    required: true
  },
  work_from: {
    type: Date,
    required: true
  },
  work_until: {
    type: Date,
    required: true
  },
  role: {
    type: String,
    required: true
  }
});
 
const Employment = mongoose.model('Employment', employmentSchema);

module.exports = Employment;
